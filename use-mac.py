import sys
import urllib2
from oauthlib.oauth2.draft25 import WebApplicationClient
from settings import *

access_token = sys.argv[1]
mac_key = sys.argv[2]

oauth = WebApplicationClient(OAUTH_CLIENT_ID, token_type='MAC',
        access_token=access_token, mac_key=mac_key,
        mac_algorithm='hmac-sha-1')

url = 'http://192.168.56.101:5000/ws/1/user/?name=lukas'
url, headers, body = oauth.add_token(url, draft=1)

request = urllib2.Request(url, headers=headers)
print urllib2.urlopen(request).read()[:70], '...'

