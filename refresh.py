import sys
import time
import urllib2
import webbrowser
from oauthlib.oauth2.draft25 import WebApplicationClient
from settings import *

refresh_token = sys.argv[1]
token_type = sys.argv[2]

oauth = WebApplicationClient(OAUTH_CLIENT_ID)

request = oauth.prepare_refresh_body(refresh_token=refresh_token,
                                     client_id=OAUTH_CLIENT_ID,
                                     client_secret=OAUTH_CLIENT_SECRET,
                                     token_type=token_type)

response = urllib2.urlopen(OAUTH_TOKEN_ENDPOINT, request).read()
oauth.parse_request_body_response(response)

print 'Access token:', oauth.access_token
print 'Expires in:', oauth.expires_in
if oauth.mac_key:
    print 'MAC key:', oauth.mac_key

