import sys
import time
import urllib2
import webbrowser
from oauthlib.oauth2.draft25 import WebApplicationClient
from settings import *

token_type = sys.argv[1]

oauth = WebApplicationClient(OAUTH_CLIENT_ID)

uri = oauth.prepare_request_uri(OAUTH_AUTHORIZE_ENDPOINT,
                                redirect_uri=OAUTH_REDIRECT_URI,
                                scope=['profile', 'tag', 'rating'])
webbrowser.open(uri)

print 'Please go the website, authorize the application and copy the authorization code.'
time.sleep(1.0)

code = raw_input('Enter authorization code: ')

request = oauth.prepare_request_body(code, redirect_uri=OAUTH_REDIRECT_URI,
                                     client_id=OAUTH_CLIENT_ID,
                                     client_secret=OAUTH_CLIENT_SECRET,
                                     token_type=token_type)

response = urllib2.urlopen(OAUTH_TOKEN_ENDPOINT, request).read()
oauth.parse_request_body_response(response)

print 'Access token:', oauth.access_token
print 'Expires in:', oauth.expires_in
if oauth.mac_key:
    print 'MAC key:', oauth.mac_key
if oauth.refresh_token:
    print 'Refresh token:', oauth.refresh_token

