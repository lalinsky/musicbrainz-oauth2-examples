import sys
import urllib2
from oauthlib.oauth2.draft25 import WebApplicationClient
from settings import *

access_token = sys.argv[1]

oauth = WebApplicationClient(OAUTH_CLIENT_ID,
        token_type='Bearer', access_token=access_token)

url = 'http://192.168.56.101:5000/ws/1/user/?name=lukas'
url, headers, body = oauth.add_token(url)

request = urllib2.Request(url, headers=headers)
print urllib2.urlopen(request).read()[:70], '...'

