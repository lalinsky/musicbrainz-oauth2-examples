# MusicBrainz OAuth Examples

## Bearer tokens

Authorize access from a desktop application:

    $ python authorize-desktop-oob.py bearer
    Please go the website, authorize the application and copy the authorization code.
    Enter authorization code: e4-NP-ewMijWt_29X47Hlw
    Access token: GKglWSIs263y7APKNeKvnQ
    Expires in: 3600
	Refresh token: IbhMBxLco8C-hjDiSDGEbw

Use the token to authenticate:

    $ python use-bearer.py GKglWSIs263y7APKNeKvnQ
    <?xml version="1.0" encoding="UTF-8"?><metadata xmlns="http://musicbra ...

Refresh the token:

    $ python refresh.py IbhMBxLco8C-hjDiSDGEbw bearer
    Access token: H-qf0zSwvyLMEcshkp3_QA
    Expires in: 3600

## MAC tokens

Authorize access from a desktop application:

    $ python authorize-desktop-oob.py mac
    Please go the website, authorize the application and copy the authorization code.
    Enter authorization code: nwFnu8ft--qKHLr-C5mplw
    Access token: H-qf0zSwvyLMEcshkp3_QA
    Expires in: 3600
	MAC key: 95nXdMCCk4IyuXwgBzD3mA 
	Refresh token: IbhMBxLco8C-hjDiSDGEbw

Use the token to authenticate:

    $ python use-mac.py H-qf0zSwvyLMEcshkp3_QA 95nXdMCCk4IyuXwgBzD3mA
    <?xml version="1.0" encoding="UTF-8"?><metadata xmlns="http://musicbra ...

Refresh the token:

    $ python refresh.py IbhMBxLco8C-hjDiSDGEbw mac
	Access token: 9fy__P6p-INx-bn7WicyjQ
	Expires in: 3600
	MAC key: Xg3PPfZP-yDgQGMC9XcgpA

